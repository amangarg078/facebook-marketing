from facebookads import FacebookAdsApi, FacebookSession
from facebookads.adobjects.adaccountuser import AdAccountUser as AdUser
from facebookads.adobjects.adaccount import AdAccount

from facebookads.adobjects.campaign import Campaign
from facebookads.adobjects.adset import AdSet
from facebookads.adobjects.ad import Ad as AdGroup
from facebookads.adobjects.adsinsights import AdsInsights
from facebookads.adobjects.abstractcrudobject import AbstractCrudObject
from facebookads.exceptions import FacebookRequestError
from facebookads.adobjects.adimage import AdImage
from facebookads.adobjects.adcreative import AdCreative
from facebookads.adobjects.targetingsearch import TargetingSearch
from facebookads.adobjects.adpreview import AdPreview


import itertools

import yaml, json, re, sys, os
import facebook
from itertools import islice
from decimal import Decimal
import logging
import yaml, json, re, sys

#FacebookAdsApi.init(my_app_id, my_app_secret, my_access_token, proxies)

def load_config(**kwargs):
    with open("accounts.yaml", 'r') as stream:
        try:
            return yaml.load(stream)
        except yaml.YAMLError as exc:
            print(exc)

def get_account(account_name):
    config = load_config()
    return config.get(account_name)


def get_proxy(config):
    return {'http':config.get('proxy'), 'https':config.get('proxy')}

def create_session(account):

    proxies = get_proxy(account)
    session = FacebookSession(account.get('app_id'), account.get('app_secret'), account.get('access_token'), proxies)
    return session

config = get_account('aman')   # Give the account name for the ad script to run for.

session = create_session(account=config)

api = FacebookAdsApi(session)
FacebookAdsApi.set_default_api(api)

me = AdUser(fbid='me', api=api)
print(me.remote_read(fields=[AdUser.Field.permissions]))
my_accounts_iterator = me.get_ad_accounts()

my_account = me.get_ad_account()

print('>>> Reading accounts associated with user')
for account in my_accounts_iterator:
    print(account)

print(">>> Campaign Stats")
for campaign in my_account.get_campaigns(fields=[Campaign.Field.name]):
    for stat in campaign.get_insights(fields=[
        'impressions',
        'clicks',
        'spend',
        'unique_clicks',
        'actions',
    ]):
        print(campaign[campaign.Field.name])
        for statfield in stat:
            print("\t%s:\t\t%s" % (statfield, stat[statfield]))


def generate_batches(iterable, batch_size_limit):
    """
    Generator that yields lists of length size batch_size_limit containing
    objects yielded by the iterable.
    """
    batch = []

    for item in iterable:
        if len(batch) == batch_size_limit:
            yield batch
            batch = []
        batch.append(item)

    if len(batch):
        yield batch


def create_multiple_website_clicks_ads(
    account,

    name,
    country,

    titles,
    bodies,
    urls,
    image_paths,


    daily_budget=None,
    lifetime_budget=None,
    start_time=None,
    end_time=None,

    age_min=None,
    age_max=None,
    genders=None,

    campaign=None,
    paused=False,
):
    # Check for bad specs
    if daily_budget is None:
        if lifetime_budget is None:
            raise TypeError(
                'One of daily_budget or lifetime_budget must be defined.'
            )
        elif end_time is None:
            raise TypeError(
                'If lifetime_budget is defined, end_time must be defined.'
            )

    # Create campaign

    if not campaign:
        print "sdsdsdsd ",account.get_id_assured()
        campaign = Campaign(parent_id=account.get_id_assured())
        campaign[Campaign.Field.name] = name + ' Campaign'
        campaign[Campaign.Field.objective] = 'LINK_CLICKS'
        campaign[Campaign.Field.status] = \
            Campaign.Status.active if not paused \
            else Campaign.Status.paused
        print "asdasdasdasdasd"
        print campaign
        campaign.remote_create()

    # Create ad set
    ad_set = AdSet(parent_id=account.get_id_assured())
    ad_set[AdSet.Field.campaign_id] = campaign.get_id_assured()
    ad_set[AdSet.Field.name] = name + ' AdSet'

    if daily_budget:
        ad_set[AdSet.Field.daily_budget] = daily_budget
    else:
        ad_set[AdSet.Field.lifetime_budget] = lifetime_budget
    if end_time:
        ad_set[AdSet.Field.end_time] = end_time
    if start_time:
        ad_set[AdSet.Field.start_time] = start_time
    targeting = {
    "geo_locations" : {
        "countries": [country]
        },
    "age_max" : age_max,
    "age_min" : age_min,
    "genders" : genders
    }
    ad_set[AdSet.Field.is_autobid] = True
    ad_set[AdSet.Field.billing_event]= 'LINK_CLICKS'
    ad_set[AdSet.Field.targeting] = targeting
    ad_set[AdSet.Field.status] = AdSet.Status.paused

    print ad_set
    ad_set.remote_create()

    # Upload the images first one by one
    image_hashes = []
    for image_path in image_paths:
        img = AdImage(parent_id=account.get_id_assured())
        img[AdImage.Field.filename] = image_path
        img.remote_create()
        image_hashes.append(img.get_hash())

    ADGROUP_BATCH_CREATE_LIMIT = 5
    ad_groups_created = []

    def callback_failure(response):
        raise response.error()

    # For each creative permutation
    for creative_info_batch in generate_batches(
        itertools.product(titles, bodies, urls, image_hashes),
        ADGROUP_BATCH_CREATE_LIMIT
    ):
        api_batch = account.get_api_assured().new_batch()

        for title, body, url, image_hash in creative_info_batch:
            # Create the ad
            ad = AdGroup(parent_id=account.get_id_assured())
            ad[AdGroup.Field.name] = name + ' Ad'
            ad[AdGroup.Field.adset_id] = ad_set.get_id_assured()
            ad[AdGroup.Field.creative] = {
                AdCreative.Field.title: title,
                AdCreative.Field.body: body,
                AdCreative.Field.object_url: url,
                AdCreative.Field.image_hash: image_hash,
            }
            ad[AdGroup.Field.status] = AdGroup.Status.paused

            ad.remote_create(batch=api_batch, failure=callback_failure)
            ad_groups_created.append(ad)

        api_batch.execute()

    return ad_groups_created


def create_website_clicks_ad(
    account,

    name,
    country,

    title,
    body,
    url,
    image_path,



    daily_budget=None,
    lifetime_budget=None,
    start_time=None,
    end_time=None,

    age_min=None,
    age_max=None,
    genders=None,

    campaign=None,
    paused=False,
):
    for ad in create_multiple_website_clicks_ads(
        account=account,

        name=name,
        country=country,

        titles=[title],
        bodies=[body],
        urls=[url],
        image_paths=[image_path],


        daily_budget=daily_budget,
        lifetime_budget=lifetime_budget,
        start_time=start_time,
        end_time=end_time,

        age_min=age_min,
        age_max=age_max,
        genders=genders,

        campaign=campaign,
        paused=paused,
    ):
        return ad


print('**** Creating ad...')

# Create my ad
my_ad = create_website_clicks_ad(
    account=my_account,

    name="Visit Seattle",
    country='US',

    title="Visit Seattle",                             # How it looks
    body="Beautiful Puget Sound.",
    url="http://www.seattle.gov/visiting/",
    image_path=os.path.join(
        os.path.dirname(__file__),
        'einstein.jpg'
    ),

    daily_budget=10000,  # $10.00 per day

    age_min=13,
    age_max=65,

    paused=True,  # Default is False but let's keep this test ad paused
)
print('**** Done!')

# Get the preview and write an html file
params = {
    'ad_format': 'RIGHT_COLUMN_STANDARD',
}
preview = my_ad.get_previews(params=params)
print(preview)


preview_filename = os.path.join(os.path.dirname(__file__), 'preview_ad.html')
preview_file = open(preview_filename, 'w')
preview_file.write(
    "<html><head><title>Facebook Ad Preview</title><body>%s</body></html>"
    % preview.get_html()
)
preview_file.close()
print('**** %s has been created!' % preview_filename)